# Blackboard Multiplex

When an assignment calls for students to upload multiple files, the ZIP
file you download will have all of those files, plus one meta-information
file per student, all in the same directory, with filenames such as
`Project 1  Student Data_n00123456_attempt_2020-09-22-21-27-46_Project1.java`.
This program finds the N numbers and original file names in these strings
and sorts the files into one directory per student, restoring their original
filenames.

Supply a directory name at the command line to process files that exist somewhere other
than the current working directory.