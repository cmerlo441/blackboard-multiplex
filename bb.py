#! /usr/bin/env python3
# -*- coding: utf-8 -*-

'''Send files downloaded from Blackboard into directories.

When an assignment calls for students to upload multiple files, the ZIP
file you download will have all of those files, plus one meta-information
file per student, all in the same directory, with filenames such as
Project 1  Student Data_n00123456_attempt_2020-09-22-21-27-46_Project1.java.
This program finds the N numbers and original file names in these strings
and sorts the files into one directory per student, restoring their original
filenames.

Supply a directory name at the command line to process files that exist
somewhere other than the current working directory.
'''

__author__ = 'Christopher R. Merlo'
__email__ = 'christopher.merlo@ncc.edu'
__version__ = 0.2
__maintainer__ = 'Christopher R. Merlo'
__copyright__ = '2020-2022 Christopher R. Merlo'
__license__ = 'MIT'
__status__ = 'Development'

import os
import re
import sys

from shutil import copyfile

directory = '.'

if(len(sys.argv) == 2):
    directory = sys.argv[1]
    print("Using", directory)
else:
    print("Using current directory")

for file in os.listdir(directory):
    # print( 'Filename:', file )
    id = re.search("n00[0-9]{6}", file)
    if(id == None):
        continue
    banner = id.group()
    fn = re.search("[^_]*$", file)
    filename = fn.group()

    # print(file)
    # print(banner, filename)

    if not os.path.exists(banner):
        os.makedirs(banner)

    source = directory + "/" + file
    target = banner + '/' + filename
    # print('Copying %s to %s' % ( source, target))
    copyfile(source,target)

